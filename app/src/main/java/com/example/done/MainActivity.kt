package com.example.done

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.activity.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.done.databinding.ActivityMainBinding
import com.example.done.databinding.TaskBinding
import com.example.done.databinding.TasksAndTitleBinding
import com.example.done.models.Task
import com.example.done.viewmodels.MainViewModel
import com.example.done.views.DoAdapter
import com.example.done.views.DoingAdapter
import com.example.done.views.DoneAdapter
import com.example.done.views.TaskFragment

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }

}