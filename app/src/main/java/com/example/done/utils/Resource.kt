package com.example.done.utils

import com.example.done.models.Task

sealed class Resource(tasks: List<Task>?) {
    data class Success(val data: List<Task>): Resource(data)
    object Loading: Resource(null)
}
