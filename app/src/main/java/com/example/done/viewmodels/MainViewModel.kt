package com.example.done.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.done.models.Task
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val _doList: MutableLiveData<MutableList<Task>> = MutableLiveData(mutableListOf())
    val doList: LiveData<MutableList<Task>> get() = _doList

    private val _doingList: MutableLiveData<MutableList<Task>> = MutableLiveData(mutableListOf())
    val doingList: LiveData<MutableList<Task>> get() = _doingList

    private val _doneList: MutableLiveData<MutableList<Task>> = MutableLiveData(mutableListOf())
    val doneList: LiveData<MutableList<Task>> get() = _doneList

    fun addDoTask(task: Task) = viewModelScope.launch(Dispatchers.Main) {
        _doList.value = doList.value.also {
            it?.add(task)
        }
    }

    fun addDoingTask(task: Task) = viewModelScope.launch(Dispatchers.Main) {
        _doingList.value = doingList.value.also {
            it?.add(task)
        }
    }

    fun addDoneTask(task: Task) = viewModelScope.launch(Dispatchers.Main) {
        _doneList.value = doneList.value.also {
            it?.add(task)
        }
    }

}