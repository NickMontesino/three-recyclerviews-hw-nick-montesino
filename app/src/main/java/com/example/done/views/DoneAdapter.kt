package com.example.done.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.done.databinding.TaskBinding
import com.example.done.models.Task

class DoneAdapter : RecyclerView.Adapter<DoneAdapter.DoneTaskViewHolder>() {

    private lateinit var tasks: List<Task>

    class DoneTaskViewHolder(private val binding: TaskBinding) : RecyclerView.ViewHolder(binding.root) {
        fun apply(task: Task) = with(binding) {
            taskBody.text = task.body
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoneTaskViewHolder {
        val binding = TaskBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DoneTaskViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DoneTaskViewHolder, position: Int) {
        val item = tasks[position]
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    fun applyTasks(taskList: List<Task>) {
        tasks = taskList
    }

}