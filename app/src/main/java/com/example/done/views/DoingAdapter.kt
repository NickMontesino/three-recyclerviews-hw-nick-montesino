package com.example.done.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.done.databinding.TaskBinding
import com.example.done.models.Task

class DoingAdapter : RecyclerView.Adapter<DoingAdapter.DoingTaskViewHolder>() {

    private lateinit var tasks: List<Task>

    class DoingTaskViewHolder(private val binding: TaskBinding) : RecyclerView.ViewHolder(binding.root) {
        fun apply(task: Task) = with(binding) {
            taskBody.text = task.body
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoingTaskViewHolder {
        val binding = TaskBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DoingTaskViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DoingTaskViewHolder, position: Int) {
        val item = tasks[position]
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    fun applyTasks(taskList: List<Task>) {
        tasks = taskList
    }

}