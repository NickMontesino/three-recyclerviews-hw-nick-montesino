package com.example.done.views

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.done.databinding.FragmentTaskBinding
import com.example.done.models.Task
import com.example.done.utils.Resource
import com.example.done.viewmodels.MainViewModel

class TaskFragment : Fragment() {

    private var _binding: FragmentTaskBinding? = null
    private val binding: FragmentTaskBinding get() = _binding!!

    private val vm by viewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentTaskBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding) {

        super.onViewCreated(view, savedInstanceState)

        doTasksContainer.taskTypeTitle.text = "Do"
        doingTasksContainer.taskTypeTitle.text = "Doing"
        doneTasksContainer.taskTypeTitle.text = "Done"

        initListeners()
        initObservers()

    }

    private fun initObservers() = with(vm) {
        doList.observe(viewLifecycleOwner) { taskList ->
            Log.d("logd", "initObservers: $taskList")
            binding.doTasksContainer.taskListContainer.adapter = DoAdapter().apply {
                applyTasks(taskList)
            }
        }
        doingList.observe(viewLifecycleOwner) { viewState ->
            binding.doingTasksContainer.taskListContainer.adapter = DoingAdapter().apply {
                applyTasks(vm.doingList.value!!)
            }
        }
        doneList.observe(viewLifecycleOwner) { viewState ->
            binding.doneTasksContainer.taskListContainer.adapter = DoneAdapter().apply {
                applyTasks(vm.doneList.value!!)
            }
        }
    }

    private fun initListeners() = with(binding) {
        addTodoButton.setOnClickListener {
            when {
                doOption.isChecked -> {
                    vm.addDoTask(Task(etTodo.text.toString()))
                    Log.d("logd", "initListeners: ${vm.doList.value}")
                }
                doingOption.isChecked -> {
                    vm.addDoingTask(Task(etTodo.text.toString()))
                }
                doneOption.isChecked -> {
                    vm.addDoneTask(Task(etTodo.text.toString()))
                }
            }
            etTodo.setText("")
        }
    }

}