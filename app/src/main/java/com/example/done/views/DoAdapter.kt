package com.example.done.views

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.done.databinding.TaskBinding
import com.example.done.models.Task

class DoAdapter : RecyclerView.Adapter<DoAdapter.DoTaskViewHolder>() {

    private lateinit var tasks: List<Task>

    class DoTaskViewHolder(private val binding: TaskBinding) : RecyclerView.ViewHolder(binding.root) {
        fun add(task: Task) = with(binding) {
            taskBody.text = task.body
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoTaskViewHolder {
        val binding = TaskBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DoTaskViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DoTaskViewHolder, position: Int) {
        holder.add(tasks[position])
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    fun applyTasks(taskList: List<Task>) {
        tasks = taskList
    }

}